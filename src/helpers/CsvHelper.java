package helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;

import models.BoolCell;
import models.DoubleCell;
import models.SpreadSheet;
import models.StringCell;

public class CsvHelper {

	public static void saveTocsv(SpreadSheet sheet) throws IOException{
		String filename = sheet.toString();
		int M = sheet.getM();
		int N = sheet.getN();
		BufferedWriter outfile = null;
		int i,j;
		String cellvalue="";
		
		filename=filename.concat(".csv");
		outfile = new BufferedWriter( new FileWriter(filename));
		for(i=0;i<M;i++){
			for(j=0;j<N-1;j++){
				cellvalue = "";
				try{
					cellvalue = sheet.getCells()[i][j].toString();
					
				}catch(Exception e){
					
				}
				cellvalue = cellvalue.concat(";");
				outfile.write(cellvalue);
			}
			cellvalue = "";
			try{
				cellvalue = sheet.getCells()[i][N].toString();
				outfile.write(cellvalue);
				outfile.newLine();
			}catch(Exception e){
				outfile.write("");
				outfile.newLine();
			}
		}
		outfile.close();
	}
	
	public static SpreadSheet getFromCsv(String filename ) throws IOException{
		String decimalPattern = "([0-9]*)\\.([0-9]*)";
		boolean check;
		SpreadSheet loadsheet =null;
		String sheetname = filename.substring(0, filename.length()-4);
		BufferedReader infile = null;
		String fileline = null;
		int x=0;
		int y=0;
		int maxy=0;
		
		String tmp;
		String values;
		int itend=0;
		int iend=0;
		
		infile = new BufferedReader(new FileReader(filename));
		while((fileline = infile.readLine()) != null){
			x++;
			y=0;
			for( int i=0; i<fileline.length(); i++ ) {
			    if( fileline.charAt(i) == ';' ) {
			        y++;
			    } 
			}
			
			if(y > maxy)
				maxy=y+1;
		}
		
		infile.close();
			
		loadsheet = new SpreadSheet(sheetname, x, maxy);
		infile = new BufferedReader(new FileReader(filename));
		int istart = 0;
		for(int i=0;i<x;i++){
			fileline = infile.readLine();
			iend = fileline.indexOf(";");
			itend = iend;
			tmp = fileline;
			for(int j=0;j<maxy-1;j++){
				values = tmp.substring(0,iend);
				itend+=1;
				tmp = fileline.substring(itend,fileline.length());
				iend = tmp.indexOf(";");
				if(iend<0)
					iend = 0;
				itend += iend;
				check = Pattern.matches(decimalPattern, values);
				if(check){
					loadsheet.addCell(new DoubleCell(i, j, Double.parseDouble(values)), i, j);
				}else{
					if(values.equals("true")){
						loadsheet.addCell(new BoolCell(i, j, true), i, j);
					}else if(values.equals("false")){
						loadsheet.addCell(new BoolCell(i, j, false), i, j);
					}else{
						loadsheet.addCell(new StringCell(i, j, values), i, j);
					}
				}
			}
			values = tmp;
			check = Pattern.matches(decimalPattern, values);
			if(check){
				loadsheet.addCell(new DoubleCell(i, maxy, Double.parseDouble(values)), i, maxy);
			}else{
				if(values.equals("true")){
					loadsheet.addCell(new BoolCell(i, maxy-1, true), i, maxy-1);
				}else if(values.equals("false")){
					loadsheet.addCell(new BoolCell(i, maxy-1, false), i, maxy-1);
				}else{
					loadsheet.addCell(new StringCell(i, maxy-1, values), i, maxy-1);
				}
			}
		}
		infile.close();

		return loadsheet;
	}

}
