package models;

public class DoubleCell extends Cell{
	
	private Double value;

	public DoubleCell(int x, int y, double value) {
		super(x, y);
		this.value = value;
	}
	
	
	@Override
	public String toString() {
		return ""+value;
	}
	
	public Double getValue() {
		return value;
	}
	

}
