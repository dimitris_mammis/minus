package models;

import java.util.ArrayList;

public class WorkBook {
	
	private String name;
	private ArrayList<SpreadSheet> sheets;
	public WorkBook(String name) {
		super();
		this.name = name;
		sheets = new ArrayList<SpreadSheet>();
	}
	
	
	public void addSheet(SpreadSheet s){
		sheets.add(s);
	}
	
	public void removeSheetByPosition(int index){
		sheets.remove(index);
	}

	
	public ArrayList<SpreadSheet> getSheets() {
		return sheets;
	}

	@Override
	public String toString() {
		return name;
	}
	
	
	
	

}
