package models;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

public class SpreadSheet {
	
	private String name;
	private Cell [][] cells;
	private int M;
	private int N;
	
	public SpreadSheet(String name, int m, int n) {
		this.name = name;
		cells = new Cell[m][n];
		M=m;
		N=n;
	}
	
	
	
	private void setCell(Cell[][] tmpCell) {
		this.cells = tmpCell;
	}
	
	public int getM() {
		return M;
	}
	
	public int getN() {
		return N;
	}
	
	public Cell[][] getCells() {
		return cells;
	}
	
	public void addCell(Cell c, int m, int n){
		cells[m][n] = c;
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}

}
