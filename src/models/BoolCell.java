package models;

public class BoolCell extends Cell{
	
	private Boolean value;

	public BoolCell(int x, int y, Boolean value) {
		super(x, y);
		this.value = value;
	}
	
	public Boolean getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return ""+value;
	}

}
