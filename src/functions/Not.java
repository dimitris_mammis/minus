package functions;

import java.util.ArrayList;

import models.Cell;

public class Not extends LogicFuncion {

	public Not(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		Boolean b1 = (Boolean) cells.get(0).getValue();
		value = !b1;
	}

}
