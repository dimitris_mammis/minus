package functions;

import java.util.ArrayList;

import models.Cell;

public class Trim extends StringFuncion {
	
	

	public Trim(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		String str = new String((String) cells.get(0).getValue());
		value = str.replace(" ", "");
	}

}
