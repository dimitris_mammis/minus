package functions;

import java.util.ArrayList;

import models.Cell;

public abstract class StatisticFunc extends FuncionCell {
	
	protected Double value;

	public StatisticFunc(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		
	}


	@Override
	public String toString() {
		return ""+value;
	}
	
	public Object getValue(){
		return value;
	}
	
}
