package functions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import models.Cell;

public class Median extends StatisticFunc {

	public Median(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		Collections.sort(cells, new Comparator<Cell>() {
	        @Override
	        public int compare(Cell  cell1, Cell  cell2)
	        {

	        	Double value1 = (Double) cell1.getValue();
	        	Double value2 = (Double) cell2.getValue();
	            return  value1.compareTo(value2);
	        }
	    });
		int position = cells.size()/2;
		value = (Double) cells.get(position).getValue();
	}

}
