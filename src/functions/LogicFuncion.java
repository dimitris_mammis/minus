package functions;

import java.util.ArrayList;

import models.Cell;

public abstract class LogicFuncion extends FuncionCell {
	
	protected Boolean value;

	public LogicFuncion(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return ""+value;
	}
	
	public Object getValue(){
		return value;
	}

}
