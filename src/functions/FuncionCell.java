package functions;

import java.util.ArrayList;

import models.Cell;

public abstract class FuncionCell extends Cell{
	
	protected ArrayList<Cell> cells;
	
	public FuncionCell(int x, int y, ArrayList<Cell> cells) {
		super(x, y);
		this.cells = cells;
	}





	public abstract void calculateValue();

}
