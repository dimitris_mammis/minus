package functions;

import java.util.ArrayList;

import models.Cell;

public class Min extends StatisticFunc {

	public Min(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		double min = (Double) cells.get(0).getValue();
		for(Cell c:cells){
			Double n =(Double)c.getValue();
			if(n < min){
				min = n;
			}
		}
		value = min;
	}

}
