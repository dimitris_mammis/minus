package functions;

import java.util.ArrayList;

import models.Cell;

public class Mult extends MathFuncion {

	public Mult(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		double mu = 1;
		for(Cell c:cells){
			mu*=(Double)c.getValue();
		}
		value = mu;
	}

}
