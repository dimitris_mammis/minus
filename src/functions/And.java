package functions;

import java.util.ArrayList;

import models.Cell;

public class And extends LogicFuncion {

	public And(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		Boolean b1 = (Boolean) cells.get(0).getValue();
		Boolean b2 = (Boolean) cells.get(1).getValue();
		value = b1 && b2;
	}

}
