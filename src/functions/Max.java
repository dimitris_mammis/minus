package functions;

import java.util.ArrayList;

import models.Cell;

public class Max extends StatisticFunc {
	

	public Max(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		double max = (Double) cells.get(0).getValue();
		for(Cell c:cells){
			Double n =(Double)c.getValue();
			if(n > max){
				max = n;
			}
		}
		value = max;
	}

}
