package functions;
import java.util.ArrayList;

import models.Cell;

public class Log extends MathFuncion {

	public Log(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		if(cells.size()==1){
			value = Math.log((Double)(cells.get(0).getValue()));
		}else{
			value = null;
		}

	}

}
