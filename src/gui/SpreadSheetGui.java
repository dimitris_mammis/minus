package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import functions.And;
import functions.FuncionCell;
import functions.Log;
import functions.Log10;
import functions.Max;
import functions.Mean;
import functions.Median;
import functions.Min;
import functions.Mult;
import functions.Not;
import functions.Or;
import functions.Remove;
import functions.Trim;
import models.BoolCell;
import models.Cell;
import models.Concat;
import models.DoubleCell;
import models.SpreadSheet;
import models.StringCell;
import models.Sum;

public class SpreadSheetGui extends JFrame {

	private JPanel contentPane;
	private JTable table;
	
	private SpreadSheet sheet;
	private JTextField fieldValue;
	private JLabel lblSelectCell;
	private JLabel lblPressRefresh;
	private JList list;
	private JButton btnAdd;
	private JButton btnLineChart;
	private JButton btnBarChart;
	private DefaultListModel<Cell> model;
	
	private ArrayList<Cell> selectedCells = new ArrayList<Cell>();
	private JButton btnClear;
	private JComboBox comboBox;
	
	
	public void setSheet(SpreadSheet sheet) {
		this.sheet = sheet;
	}
	
	private void refreshModel(){
		String str[] = new String [sheet.getN()];
		for(int i=0;i<sheet.getN();i++){
			str[i] = ""+i;
		}
		DefaultTableModel model = new DefaultTableModel(sheet.getCells(),str); 
		table.setModel(model);
	}
	
	@Override
	public void show() {
		super.show();
		refreshModel();
	}
	
	private void newCell(){
		
		int row = table.getSelectedRow();
		int column = table.getSelectedColumn();
		
		String text = fieldValue.getText();
		
		Cell c = null;
		
		try{
			double d = Double.parseDouble(text);
			c = new DoubleCell(row, column, d);
		}catch(Exception e){
			if(text.equals("true")){
				c = new BoolCell(row, column, true);
			}else if(text.equals("false")){
				c = new BoolCell(row, column, false);
			}else{
				c = new StringCell(row, column, text);
			}
		}
		
		sheet.addCell(c, row, column);
		refreshModel();
		
	}
	
	
	private void addSelectedCell() {
		int row = table.getSelectedRow();
		int column = table.getSelectedColumn();
		
		selectedCells.add(sheet.getCells()[row][column]);
		model.addElement(sheet.getCells()[row][column]);
		
	}
	
	
	public DefaultCategoryDataset getData(){
		
		DefaultCategoryDataset objDataset = new DefaultCategoryDataset();
		int count = 0;
		for(Cell c:selectedCells){
			objDataset.setValue((Double)c.getValue(),"Category 1",""+count);
			count++;
		}
		System.out.println(selectedCells.toString());
		System.out.println(objDataset.toString());
		System.out.println(count);
		return objDataset;
	}
	
	public String getXAxis(){
		return "x";
	}
	
	public String getYAxis(){
		return "y";
	}
	
	private void linePlot() {
		JFreeChart objChart = ChartFactory.createLineChart(
			       getTitle(),     //Chart title
			    getXAxis(),     //Domain axis label
			    getYAxis(),         //Range axis label
			    getData(),         //Chart Data 
			    PlotOrientation.VERTICAL, // orientation
			    false,             // include legend?
			    true,             // include tooltips?
			    false             // include URLs?
			);
		
		ChartFrame frame = new ChartFrame("Chart", objChart);
		frame.pack();
		frame.setVisible(true);
		
	}
	
	private void barplot() {
		
		
		JFreeChart objChart = ChartFactory.createBarChart(
			       getTitle(),     //Chart title
			    getXAxis(),     //Domain axis label
			    getYAxis(),         //Range axis label
			    getData(),         //Chart Data 
			    PlotOrientation.VERTICAL, // orientation
			    false,             // include legend?
			    true,             // include tooltips?
			    false             // include URLs?
			);
		
		ChartFrame frame = new ChartFrame("Chart", objChart);
		frame.pack();
		frame.setVisible(true);
		
	}
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SpreadSheetGui frame = new SpreadSheetGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SpreadSheetGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 724, 650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		table = new JTable();
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				newCell();
				
				
				
			}
		});
		
		JLabel lblSelectCellAnd = new JLabel("1. Write value here");
		
		fieldValue = new JTextField();
		fieldValue.setColumns(10);
		
		lblSelectCell = new JLabel("2. Select cell");
		
		lblPressRefresh = new JLabel("3. Press refresh");
		
		model = new DefaultListModel<Cell>();
		list = new JList(model);
		
		btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addSelectedCell();
			}

			
		});
		
		btnLineChart = new JButton("Line chart");
		btnLineChart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				linePlot();
			}
		});
		
		btnBarChart = new JButton("Bar chart");
		btnBarChart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				barplot();
			}
		});
		
		btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedCells.clear();
				model.clear();
			}
		});
		
		JLabel lblSelectedValues = new JLabel("Selected values");
		
		comboBox = new JComboBox();
		comboBox.addItem(new String("and"));
		comboBox.addItem(new String("or"));
		comboBox.addItem(new String("not"));
		comboBox.addItem(new String("log10"));
		comboBox.addItem(new String("log"));
		comboBox.addItem(new String("sum"));
		comboBox.addItem(new String("min"));
		comboBox.addItem(new String("max"));
		comboBox.addItem(new String("mean"));
		comboBox.addItem(new String("median"));
		comboBox.addItem(new String("mult"));
		comboBox.addItem(new String("trim"));
		comboBox.addItem(new String("concat"));
		comboBox.addItem(new String("remove"));
		
		
		JButton btnAccFunction = new JButton("Add Function");
		btnAccFunction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedItem = (String)comboBox.getSelectedItem();
				FuncionCell f = null;
				
				int row = table.getSelectedRow();
				int col = table.getSelectedColumn();
				
				
				if(selectedItem.equals("or")){
					f = new Or(row, col, selectedCells);
				}else if(selectedItem.equals("not")){
					f = new Not(row, col, selectedCells);
				}else if(selectedItem.equals("and")){
					f = new And(row, col, selectedCells);
				}else if(selectedItem.equals("max")){
					f = new Max(row, col, selectedCells);
				}else if(selectedItem.equals("log10")){
					f = new Log10(row, col, selectedCells);
				}else if(selectedItem.equals("sum")){
					f = new Sum(row, col, selectedCells);
				}else if(selectedItem.equals("mult")){
					f = new Mult(row, col, selectedCells);
				}else if(selectedItem.equals("log")){
					f = new Log(row, col, selectedCells);
				}else if(selectedItem.equals("trim")){
					f = new Trim(row, col, selectedCells);
				}else if(selectedItem.equals("remove")){
					f = new Remove(row, col, selectedCells);
				}else if(selectedItem.equals("concat")){
					f = new Concat(row, col, selectedCells);
				}else if(selectedItem.equals("mean")){
					f = new Mean(row, col, selectedCells);
				}else if(selectedItem.equals("min")){
					f = new Min(row, col, selectedCells);
				}else if(selectedItem.equals("median")){
					f = new Median(row, col, selectedCells);
				}
				f.calculateValue();
				sheet.addCell(f, row, col);
				refreshModel();
				
				
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(table, GroupLayout.PREFERRED_SIZE, 682, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPressRefresh)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblSelectCellAnd)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(fieldValue, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblSelectCell)
								.addComponent(btnRefresh))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(list, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(btnAccFunction)
										.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(btnClear)
										.addComponent(btnAdd)
										.addComponent(btnBarChart)
										.addComponent(btnLineChart)))
								.addComponent(lblSelectedValues))))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 306, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addComponent(lblSelectCellAnd)
											.addGap(13)
											.addComponent(lblSelectCell))
										.addComponent(fieldValue, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblSelectedValues)))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(67)
									.addComponent(lblPressRefresh)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(btnRefresh)))
							.addContainerGap(188, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(list, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnAdd)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnClear)
									.addGap(46)
									.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(1)
									.addComponent(btnAccFunction)
									.addPreferredGap(ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
									.addComponent(btnBarChart)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(btnLineChart)))
							.addContainerGap())))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
